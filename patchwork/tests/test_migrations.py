# The MIT License (MIT)
#
# Copyright (c) 2020 Arkadiusz Hiler
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from patchwork.models import Comment, Patch, Person, Series
from patchwork.tests.utils import defaults
from patchwork.migration_helpers import deduplicate_persons

from django.test import TestCase


class TestDeduplicatePersons(TestCase):
    fixtures = ['default_states', 'default_events']

    def setUp(self):
        defaults.project.save()
        self.p1 = Person(email="arkadiusz@example.com", name="Arek")
        self.p2 = Person(email="x@example.com", name="Arek")
        self.p1.save()
        self.p2.save()

        # let's create the conflict skipping the regular validaiton
        Person.objects.filter(email="x@example.com") \
                      .update(email='Arkadiusz@example.com')
        self.assertEqual(2, Person.objects.count())

    def testUnifiesCorrectly(self):
        deduplicate_persons(Person)
        self.assertEqual(1, Person.objects.count())

    def testDoesNotRemoveRelateSeries(self):
        Series(project=defaults.project, submitter=self.p1).save()
        Series(project=defaults.project, submitter=self.p2).save()

        deduplicate_persons(Person)
        person = Person.objects.get()

        for s in Series.objects.all():
            self.assertEqual(s.submitter, person)

    def testDoesNotRemoveRelatePatchesAndComments(self):
        patch1 = Patch(msgid="1", project=defaults.project, submitter=self.p1)
        patch2 = Patch(msgid="2", project=defaults.project, submitter=self.p2)

        patch1.save()
        patch2.save()

        Comment(patch=patch1, submitter=self.p2).save()
        Comment(patch=patch2, submitter=self.p1).save()

        deduplicate_persons(Person)
        person = Person.objects.get()

        for p in Patch.objects.all():
            self.assertEqual(p.submitter, person)

        for c in Comment.objects.all():
            self.assertEqual(c.submitter, person)
